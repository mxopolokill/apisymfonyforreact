<?php
namespace App\DataFixtures;

use App\Entity\Categories;
use Faker\Factory;
use App\Entity\User;
use App\Entity\Expenses;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture 
{
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
       $this->encoder = $encoder; 
    }
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 5; $i++) {

            $user = new User();
    
            $user->setEmail($faker->email())
                 ->setPseudo($faker->name());
    
            $password = $this->encoder->encodePassword($user, 'password');
            $user->setPassword($password);
    
            $manager->persist($user);
        
            for ($j=0; $j < 5; $j++) {

                $category = new Categories();

                $category->setName($faker->word())
                         ->setIcon('<i class="fas fa-euro-sign"></i>')
                         ->setColor($faker->colorName());
                
                $manager->persist($category);

                for($k=0; $k < 5; $k++) {
                    
                    $expense = new Expenses();
            
                    $expense->setTitle($faker->words(3, true))
                            ->setDescription($faker->text(100))
                            ->setAmount($faker->numberBetween(1, 1000))
                            ->setPaymentMethod($faker->creditCardType())
                            ->setPhoto($faker->imageUrl(360, 360, 'animals', true, 'cats'))
                            ->setUser($user)
                            ->setCategory($category);
        
                    $manager->persist($expense);
                }
            }

        }

        $manager->flush();
    }
}
