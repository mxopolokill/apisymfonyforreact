<?php

namespace App\Repository;

use App\Entity\Expenses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Expenses|null find($id, $lockMode = null, $lockVersion = null)
 * @method Expenses|null findOneBy(array $criteria, array $orderBy = null)
 * @method Expenses[]    findAll()
 * @method Expenses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpensesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Expenses::class);
    }

    public function findLastMonth($id)
    {
        $date = new \DateTime();
        $date->modify('-30 days');

        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter('user', $id)
            ->andWhere('e.CreatedAt > :date')
            ->setParameter(':date', $date)
            ->orderBy('e.CreatedAt', 'DESC')
            ->getQuery();

        return $qb->getResult();
    }

    public function findMonth($id, $year, $month)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter('user', $id)
            ->andWhere('YEAR(e.CreatedAt) = :year')
            ->setParameter(':year', $year)
            ->andWhere('MONTH(e.CreatedAt) = :month')
            ->setParameter(':month', $month)
            ->orderBy('e.CreatedAt', 'DESC')
            ->getQuery();

        return $qb->getResult();
    }

    public function findCatExpenses($id, $cat)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter('user', $id)
            ->andWhere('e.category = :cat')
            ->setParameter(':cat', $cat)
            ->orderBy('e.CreatedAt', 'DESC')
            ->getQuery();

        return $qb->getResult();
    }

    public function findCatMonthExpenses($id, $cat, $year, $month)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter('user', $id)
            ->andWhere('e.category = :cat')
            ->setParameter(':cat', $cat)
            ->andWhere('YEAR(e.CreatedAt) = :year')
            ->setParameter(':year', $year)
            ->andWhere('MONTH(e.CreatedAt) = :month')
            ->setParameter(':month', $month)
            ->orderBy('e.CreatedAt', 'DESC')
            ->getQuery();

        return $qb->getResult();
    }

    public function findAllExpenses($id)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.user = :user')
            ->setParameter('user', $id)
            ->orderBy('e.CreatedAt', 'DESC')
            ->getQuery();

        return $qb->getResult();
    }

    // public function findAllExpenses($id)
    // {
    //     $qb = $this->createQueryBuilder('e')
    //         ->select('e')
    //         ->where('e.user = :user')
    //         ->setParameter('user', $id)
    //         ->orderBy('e.CreatedAt', 'DESC')
    //         ->getQuery();

    //     return $qb->getResult();
    // }

    // /**
    //  * @return Expenses[] Returns an array of Expenses objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Expenses
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
