<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ExpensesRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ExpensesRepository::class)
 */
#[ApiResource(
    normalizationContext: ['groups' => ['read']],
)]

class Expenses
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(["read"])]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["read"])]
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    #[Groups(["read"])]
    private $description;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Groups(["read"])]
    private $CreatedAt;

    /**
     * @ORM\Column(type="integer")
     */
    #[Groups(["read"])]
    private $amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(["read"])]
    private $paymentMethod;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(["read"])]
    private $photo;

    /**
     * @ORM\ManyToOne(targetEntity=Categories::class, inversedBy="expenses")
     */
    #[Groups(["read"])]
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="expenses")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(["read"])]
    private $user;

    public function __construct()
    {
        $this->setCreatedAt(new DateTimeImmutable());
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeImmutable $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getCategory(): ?Categories
    {
        return $this->category;
    }

    public function setCategory(?Categories $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
