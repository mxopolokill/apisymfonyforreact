<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\ExpensesRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LastMonthExpensesController extends AbstractController
{
    #[Route('/api/user/{id}/lastmonth', name: 'last_month_expenses')]
    public function lastMonthExpenses(ExpensesRepository $repo, SerializerInterface $serializer, $id): Response
    {
        $expenses = $repo->findLastMonth($id);
        
        $get = $serializer->serialize($expenses, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }

    #[Route('/api/user/{id}/{year}/{month}', name: 'month_expenses')]
    public function monthExpenses(ExpensesRepository $repo, SerializerInterface $serializer, $id, $year, $month): Response
    {
        $expenses = $repo->findMonth($id, $year, $month);
        
        $get = $serializer->serialize($expenses, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }

    #[Route('/api/user/{id}/{cat}', name: 'cat_expenses')]
    public function catExpenses(ExpensesRepository $repo, SerializerInterface $serializer, $id, $cat): Response
    {
        $expenses = $repo->findCatExpenses($id, $cat);
        
        $get = $serializer->serialize($expenses, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }

    #[Route('/api/user/{id}/{cat}/{year}/{month}', name: 'cat_month_expenses')]
    public function catMonthExpenses(ExpensesRepository $repo, SerializerInterface $serializer, $id, $cat, $year, $month): Response
    {
        $expenses = $repo->findCatMonthExpenses($id, $cat, $year, $month);
        
        $get = $serializer->serialize($expenses, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }

    #[Route('/api/user/{id}', name: 'all_expenses')]
    public function allExpenses(ExpensesRepository $repo, SerializerInterface $serializer, $id): Response
    {
        $expenses = $repo->findAllExpenses($id);
        
        $get = $serializer->serialize($expenses, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }
    
    #[Route('/api/user/{id}/{year}/{month}/sum', name: 'month_expenses_sum')]
    public function monthExpensesSum(ExpensesRepository $repo, SerializerInterface $serializer, $id, $year, $month): Response
    {
        $expenses = $repo->findMonthSum($id, $year, $month);
        
        $get = $serializer->serialize($expenses, 'json');

        $response = new Response($get, 200, [
            "Content-type" => "application/json"
        ]);

        return $response;
    }
}
