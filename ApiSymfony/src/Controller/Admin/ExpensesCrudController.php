<?php

namespace App\Controller\Admin;

use App\Entity\Expenses;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ExpensesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Expenses::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            TextEditorField::new('description'),
            Field::new('CreatedAt'),
            Field::new('amount'),
            Field::new('paymentMethod'),
            Field::new('photo'),
            AssociationField::new('category')
                ->autocomplete(),
            AssociationField::new('user')
                ->autocomplete(),
        ];
    }
}
