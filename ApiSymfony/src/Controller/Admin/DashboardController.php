<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Expenses;
use App\Entity\Categories;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('ApiSymfonyExpenses');
    }

    public function configureMenuItems(): iterable
    {
            yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
            yield MenuItem::linkToCrud('Expenses', 'fa fa-euro-sign', Expenses::class);
            yield MenuItem::linkToCrud('Categories', 'fa fa-file-invoice-dollar', Categories::class);
            yield MenuItem::linkToCrud('Users', 'fa fa-user', User::class);
    }
}
